from functools import wraps
from flask import abort
from flask_login import current_user

""" NOTE: """
""" Decorador que comprueba si el objeto current_user tiene atributo is_admin """
""" Si tiene el atributo id_admin y su valor es True ejecuta la función f """
def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        is_admin = getattr(current_user, 'is_admin', False)
        if not is_admin:
            abort(401)
        return f(*args, **kwargs)
    return decorated_function