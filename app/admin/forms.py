from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, BooleanField
from wtforms.validators import DataRequired, Length

"""cab blog"""
class PostForm(FlaskForm):
    title = StringField("Título", validators=[DataRequired(), Length(max=128)])
    title_slug = StringField("Título slug",validators=[DataRequired(), Length(max=128)])
    content = TextAreaField("Contenido")
    submit = SubmitField("Guardar")

class UserAdminForm(FlaskForm):
    is_admin = BooleanField('Administrador')
    submit = SubmitField('Guardar')

