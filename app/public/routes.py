import logging
from flask import abort, render_template, current_app
from werkzeug.exceptions import NotFound
from app.models import Post
from . import public_bp
from werkzeug.exceptions import NotFound

""" importar modulo loggin para crear looger """
logger = logging.getLogger(__name__)

@public_bp.route("/")
def index():
    current_app.logger.info("Mostrando los posts del blog")
    logger.info("Mostrar los post del blog")
    posts = Post.get_all()
    return render_template("public/index.html", posts=posts)

@public_bp.route("/p/<string:slug>/")
def show_post(slug):
    logger.info("Mostrando un post")
    logger.debug(f"Slug : {slug}")
    post = Post.get_by_slug(slug)
    if post is None:
        # abort(404)
        logger.info(f"El post {slug} no existe")
        raise  NotFound(slug)
    return render_template("public/post_view.html", post=post)

@public_bp.route("/error")
def show_error():
    res = 1 / 0
    posts = Post.get_all()
    return render_template("public/index.html", posts=posts)