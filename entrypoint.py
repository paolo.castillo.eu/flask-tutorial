import os
from app import create_app

# EJ: APP_SETTINGS_MODULE = "config.ProductionConfig"

setting_module= os.getenv("APP_SETTINGS_MODULE")
app = create_app(setting_module)