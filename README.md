# Lección del tutorial
- Lección 1: La primera app de Flask
- Lección 2: Plantillas
- Lección 3: Formularios
- Lección 4: Login
- Lección 5: Base de datos
- Lección 6: Estructura de proyecto Blueprints
- Lección 7: Parametros de configuración de proyecto
- Lección 8: Gestión y manejo de errores y excepciones
- Lección 9: Logs
- Lección 10: Seguridad a la vista

# Descripción
Flask es una instancia WSGI de la clase Flask al crear la instancia llama app, donde dicha   instancia se utilizara la palabra `__name__` Esto se necesario para que Flask sepa, donde encontrar las plantilla de nuestra aplicación o fiche estáticos

# Caracteristicas
1. Una caracteristicas de Flask que se utiliza métodos asociados a las distintas URLs que compone la app
2. El decorador de route de la aplicación (app) es el encargado de decirle a Flask qué URL debe ejecutar
3. El nombre que le demos a la función será usado para genera dichas URLs.
4. Finalmente, dicha petición a la URL de la función devolvera una respuesta
5. Para el lanzamiento de la aplicación se debe ejecutar lo siguiente en el servidorVariable de entorno:

```
Linux y Mac = export FLASK_APP="entrypoint.py"
Windows = set "FLASK_APP=entrypoint.py"
Windows PowerShell = $env:FLASK_APP="entrypoint.py"

```

Una vez definido dónde puede el servidor Flask encontrar la aplicación, se debe ejecutar:

```
$ flask run
o
$ python -m flask run
```

Al lanzar la aplicación de flask, lanzara lo siguiente

```
* Serving Flask app "entrypoint.py"
* Environment: production
  WARNING: Do not use the development server in a production environment.
  Use a production WSGI server instead.
* Debug mode: off
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Al probar la url que indica en la temrinal, el navegador muestra el mensaje de <<Hello World!>> de la función `hello_world()`. Por defecto el servidor escucha por el puerto 5000 y solo acepta peticiones de nuestro ordenador. Si se desea cambiar por cualquier motivo:

- Establecer variable de entorno `FLASK_RUN_PORT` en un puerto diferente.
- Indicar el puerto al lanzar la aplicación `flask run --port 6000`

Para aceptar otros ordenadores a nuestra red, lanzamos el servidor de la siguiente manera:

```
$ flask run --host 0.0.0.0
```

## Modo debug

Flask viene con un modo debug, en lo cual cuando se este desarrollando y se realiza un modificación el servidor de reinicia automaticamente, si la necesidad de hacerlo manual.

Para activar el modo debug, simplemente hay que añadir la variable de entorno `FLASK_ENV` y asignar el valor `development`.

En linux:
```
$ export FLASK_ENV="development"
```
En windows:
```
> set "FLASK_ENV=development" 
> $env:FLASK_ENV="development"
```

==NO USAR EL MODO DEBUG EN ENTORNO PRODUCTIVO==

Al ejecutar el modo debug se obtendra lo siguiete por terminal:

```
* Serving Flask app "entrypoint.py" (lazy loading)
* Environment: development
* Debug mode: on
* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
* Restarting with stat
* Debugger is active!
* Debugger PIN: 884-057-557
```

## Conversore

Por defecto, en flask existen los siguientes conversores:

- string: Es el conversor por defecto. Acepta cualquier cadena que no contenga el carácter '/'.
- int: Acepta números enteros positivos.
- float: Acepta números en punto flotante positivos.
- path: Es como string pero acepta cadena con el carácter '/'.
- uuid: Acepta cedenas con formato UUID.

## Formar URLs en nuesstro código
Flask pone a disposición el método `url_for()` para componer una URL a partir del nombre de una vista. Esta función capeta como parámetro el nombre de una vista y un número variable de argumentos clave-valor, cada uno de ellos asociado a una parte vairable de URL. Si se pasa un argumento cuyo nombre no se corresponde con un parámetro de la parte variable, se añade a la URL como parte de la query string:

```
>>> print(url_for("index"))
/
>>> print(url_for("show_post", slug="leccion-1", preview=True))
/p/leccion-1/?preview=True
```

Es recomendable usar el método `url_for()` en lugar de codificar directamente la URL en el código, por el siguiente motivo:

- Si se cambia la URL en cualquier momento, al no estar harcodeada, esta modificación no afecta al código.
- La función escapa caracteres especiales, como los esacios.
- Las rutas generadas son siempre absolutas, evitándo las rutas relativas.
- Si la aplicación se sitúa bajo un contexto (ej: /miniblog), en lugar de la ruta raíz `/`, `url_for()` gestionará esta situació por nosotros.

## Manejando los formulario con flask-WTF

Para instalar las extensión de Flask-WTF, se debe ejecutar lo siguiete:

```
$ pip install Flask-WTF
$ pip install email-validator
```

Los formulario de registro con flask-WTF se representa mediante una clase. Esta clas hereda del objeto `FlaskForm` y en ella se definen los campos del formulario como variables de clase.

Detalle que requiere una mención especial es la línea `{{ form.hidden_tag() }}`. Esto lo que hace es añadir todos los campos tipo hidden del formulario si los hubiera. Por defecto, Flask-WTF genera para todas las instancias de la clase FlaskForm un campo oculto que contiene un token y sirve para proteger nuestra aplicación contra ataques CSRF.

## Manejo de login con Flask-login

Para implementar el login de usuario en flask, se utilizara flask-login. Siempre que se pueda, no hay que reinventar la rueda. ¿Que nos ofrece Flask-login?:

- Almacenar el ID del usuario en la sesión y mecanismos para hacer login y logout
- Restringir el acceso a ciertas vista unicamente a los usuario autenticados
- Gestionar la funcionalidad `Recuérdame` para mantener la sesión incluso después de que el usuario cierre el navegador.
- Proteger el acceso a las cookies de sesión frente a terceros.

Para instalar dicha libreria en la aplicación se debe ejecutar lo siguiete en consola:

```
$ pip install flask-login
```

Dado que Flask-login hace uso de la sesión para autentificación, debemos establecer la variable de configuración `SECRET_KEY`.

La extensión `Flask-login` te da la libertad de definir tu clase para el manejo de usuario. Esto hace posible que se pueda utilizar en cualquier sistema de base de datos y que modifiquemos el modelo en función de las necesidades de usuario, la clase se debe implementar las siguiente propiedades y métodos como obligación:

- `is_authenticated`: propiedad que es `True` si el usuario se ha autentificado y `False` en caso contrario.
- `is_active`: una propiedad que indica si la cuenta del usuario esta activa (`True`) o no (`False`).
- `is_anonymous`: una propiedad que vale `False` para los usuarios reales y `True` para los usuario anónimos.
- `get_id()`: un método que devuelve un string(`unicode` en caso de python2) con el `ID` único del usuario. Si el `ID` del usuario fuera `int` o cualquier otro tipo, es responsabilidad de uno convertirlo a `string`.

## Flask SQLAlchemy

En la actualidad existen, principalmente, dos grandes familias de base de datos: las relacionales(postgresSQL, MySql, Oracle) y la NoSQL (MongoDB, CouchDB, ...). Utilizar un tipo u otro en nuestra aplicación dependerá de multiples factores, como la escalabilidad o asegurar la fiabilidad de los datos.

### Flask-SQLAlchemy: un ORM para Flask

Un ORM (Object-Relational Mapper) es un toolkit que nos ayuda a trabajar con las tablas de la base de datos como si fueran objetos, de manera que cada tabla se mapea con una clase y cada columna con un campo de dicha clase. Además, también nos permite mapear las realaciones entre tablas como relaciones entre objetos.

No obstante, una de las características que más atractivos hacen a los ORMs es que puedes cambiar tu base de datos sin apenas modificar código. De manera que puedes programar tu aplicación con un ORM sin pensar que la base de datos es : PostgreSQL o MySQL.

### Configurar Flask-SQLAlchemy

Al igual que con otras extensiones, lo primero que se debe realizar para usar el motor de flask SQL es intalar. Para ello, ejecuta lo siguiente:

```
$ pip install flask-sqlalchemy
```

Para el caso de este proyecto, se realizara con PostgreSQL para el lenguaje Python implementado utilizado `libpq`,la libreria oficinal del cliente PostgreSQL. Para instalar:

```
$ pip install psycopg2
```

### Guardando el modelo y haciendo consulta con Flask-SQLAlchemy

En flask-SQLAlchemy, para guardar un objeto en una base de datosm este debe estar previamente asociado al objeto `Colum`. Una vez asociado, para que los cambios se vean reflejados en la base de datos hay que hacer un `commit`. Todo esto se realiza en el método `save()` de la clase.
Lo último, para consultaar a una base de datos se heredan de clase `Model`, disponen del objeto `query`, a travéz del cuál se realizan todas las consultas a la base de datos.

Para crear la tabla dentro de una db, se debe ejecutar lo siguiente, en base al archivo run:

```
>>> from run import db
>>> db.create_all()
```
### Blueprints

Básicamente blueprints define una colección de vista, plantillas, recursos estaticos, modeleo, etc. para que pueda ser utilizados por la aplicación. 

### Divisón estructural

Mediante una divisón estructura, se caracteriza por agrupar código por vista, plantilla, modelos, formulario, etc. en diferentes módulos. De este modo, una aplicación cualquiera tendría la siguiente estructura:

```
+ mi_proyecto/
   |_ app/
      |_ __init__.py
      |_ static/
      |_ templates/
         |_ public/
            |_ index.html
            |_ ...
         |_ users/
            |_ login.html
            |_ sign_up.html
            |_ ...
         |_ private/
            |_ index.html
         |_ ...
      |_ routes/
         |_ __init__.py
         |_ private.py
         |_ public.py
         |_ users.py
         |_ ...
      |_ models/
         |_ users.py
         |_ ...
      |_ forms/
         |_ users.py
         |_ ...
   |_ entrypoint.py
   |_ requirements.txt
   |_ ...
```

### Divisón funcional

Con una devisión funcional se caracteriza por agrupar en distintos componente según los requisitos funcionales. De esta forma, todas las vista, plantillas, modelos, fomularios, etc. se realaciona en la aplicación dentro de un mismo paquete:

```
+ mi_proyecto/
   |_ app/
      |_ __init__.py
      |_ public/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ private/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ users/
         |_ __init__.py
         |_ routes.py
         |_ static/
         |_ templates/
         |_ models.py
         |_ forms.py
         |_ ...
      |_ ...
      |_ static/
      |_ templates/
   |_ entrypoint.py
   |_ requirements.txt
   |_ ...
```

# Reorganizando miniblog en Blueprints

La estructura establecida para este proyecto es mediante las funcionlidades a desarrollar en distintos módulos por medio de Blueprints. El resultado final será algo parecido a la estructura explicada anterior.

```
+ miniblog
|_ app
   |_ __init__.py
   |_ models.py
   |_ admin/
      |_ __init__.py
      |_ forms.py
      |_ routes.py
      |_ templates/
         |_ admin/
            |_ post_form.html
   |_ auth/
      |_ __init__.py
      |_ forms.py
      |_ models.py
      |_ routes.py
      |_ templates/
         |_ auth/
            |_ login_form.html
            |_ signup_form.html
   |_ public/
      |_ __init__.py
      |_ routes.py
      |_ templates/
         |_ public
            |_ index.html
            |_ post_view.html
   |_ static/
      |_ base.css
   |_ templates/
      |_ base_template.html
|_ env/
|_ .gitignore
|_ CHANGELOG.md
|_ entrypoint.py
|_ LICENSE
|_ README.md
|_ requirements.txt
```

![alt text](estructura.png)

# Técnicas para separar los parámetros de configuración en función del entorno

- Local: Es tu propio entorno, tu ordenador, donde desarrollas el código. Cada uno de los programadores tiene su entorno local.
- Desarrollo: Es un entorno compartido en el que todos los programadores tienen acceso. Se suele usar para probar los cambios que se están haciendo durante el desarrollo de la aplicación.
- Staging: Es el entorno de preproducción. Un entorno de pruebas lo más parecido posible a producción. Generalmente es un entorno estable, sin fallos, que se suele usar para que los clientes puedan probar la aplicación de forma independiente al entorno de desarrollo.
- Test: Es el entorno en el que se ejecutan los diferentes test.
- Producción: Este es el entorno real donde se despliega la aplicación para su uso por parte de los clientes y usuarios

# Consejos prácticos

- Separar las configuraciones de la app con diferentes ficheros, uno por entorno.
- Indica el fichero de configuración a utilizar con una variable de entorno.
- Usa el directorio instance para definir aquellos parámetros que no deben formar parte del sistema de control de versiones.
- Define las vairables de entorno `FLASK_ENV` y `FLASK_DEBUG` en lugar de usar los parámetros `ENV` y `DEBUG`, respectivamente.
- Recordar arrancar la aplicación con los parametros de inicio.
- No escribir código que necesite de parámetros de configuraciones en tiempo de importanción. Si fuera necesario, reescribe el código para que pueda usarlos en un momento posterior.
- En este caso, los valores `development` y `production` que pueda tener el parámetro `ENV` son insuficientes.

```
! Recuerda definir la variable de entorno APP_SETTING_MODULE con el valor del entorno en el que este ejecutando la aplicación

$env:APP_SETTINGS_MODULE="config.local"
$set "APP_SETTINGS_MODULE=config.local"
$export APP_SETTINGS_MODULE="config.local"
```

# Manejadores de errores

Como su nombre lo indica, los manejadores de errores son funciones que se ejecutan cuando se produce una situación de errores, Son muy útiles porque nos permiten hacer multitud de cosas cuando ocurre un error: escrbiri en un log, mostrar una pagina de error personalidadas, añadir una cabecera a la respuesta, etc.

Formas de difinir un error:

- Decorando una función con `errorhandler()`
- registrando una función como errohandler: `app.register_error_handler(codigo_estado, funcion)`

Se puede manejar dos tipos de errores:

- Códigos de estado HTTP conocidos
- Excepciones

# Qué sucede cuando hay un error no controlado

En el caso de una aplicación web, lo más común cuando ocurre un error no controlado es que al usuario se le muestre una página de error, acompañada de un código de estado HTTP. Estas páginas de error bien las serve la propia aplicación, bien el servidor werb en función del código de estado devuelto por la respuesta de la aplicación.

- 200: La petición se procesó correctamente.
- 4xx: Hubo un problema relacionado con la petición. Normalmente relacionados con el cliente o el usuario.
- 5xx: Hubo un problema en la parte del servidor. Suelen ser fallos en la aplicación.

# Configuraciones por defecto de los logs

Para escribir un mensaje de los logs se puede usar el `logger` asociado al objeto `app` de la aplicación Flask (app.logger). Este `logger` toma como nombre el de la propia aplicación. Además, es el que usa Flask para mostrar sus propios mensajes de logs.

Los mensajes de los logs se pueden clasificar en distintos niveles según el tipo de información que ofrecen. Los niveles más comunes son:

- DEBUG: Para depurar información. Nivel de información muy detallado.
- INFO: Para mensajes informativos que indican que la aplicación se está ejecutando correctamente.
- WARNING: Para mensajes de advertencia cuando la aplicación, aun funcionando correctamente, detecta una situación inesperada o posible problema futuro.
- ERROR: Para mensajees de errrores.
- EXCEPTION: Para mensajes de error que se producen debido a una excepción. Se muestra la traza de la excepción.

# Forma menos buena de añadir seguridad a la vista

En la siguiente sección se realizara una forma de mejorar la seguridad de la vista utilizando decoradores en ellas, enfocandoce directamente al app.admin (usuario administrador).

## El uso de decoradores para añadir seguridad en las vistas.

Básicamente un decorado es una función que recibe como parámetro otra función y devuelve como resultado una función diferente, es decir: Permite que una función envuelva a otra función, ejecutando código previo a la ejecución de esta última. Resultando muy útiles para eliminar código repetitivo, seprando las responsabilidad del código, añadiendo seguridad a la vista y aumentar su legibilidad.

Existen varias forma de crear un decorador, pero la mas simple es un decorador sin parámetros, sigue la siguiente estructura:

```
from functools import wraps

def mi_decorador(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        # Código del decorador
        ...
        return f(*args, **kws)
    return decorated_function
```

En la código anterior se puede visualizar en `mi_decorador` recibe como parametro de entrada la función `f` y devuelve como resultado una función `decorated_function` (que es el resultado de ejecutar el decorador).

### Ejemplo de decorador

```
def print_nombre(nombre):
    print(nombre)

def print_hola(f):
    print('Hola')
    f
```

Como se puede apreciar, la segunda recibe como parámetro una función. Si las concatenamos del siguiente modo, el resultado sería la ejecución de `print_nombre` más el resultado de `print_hola`:

```
print_hola(print_nombre('j2logo'))
j2logo
Hola
```
Si ahora transformamos la función `print_hola` en un decorador el resultado sería diferente, puesto que primero se ejecuta el código del decorador como envoltorio de la función `print_nombre()`

```
from functools import wraps

def print_hola(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        print('Hola')
        return f(*args, **kws)
    return decorated_function

@print_hola
def print_nombre(nombre):
    print(nombre)


>>> print_nombre('j2logo')
Hola
j2logo
```
Al decorar la función `print_nombre()`, se ejecuta el código del decorador antes que el de la propia función.
