from .default import *

APP_ENV = APP_ENV_STAGING

SQLALCHEMY_DATABASE_URI = 'postgresql://db_user:db_pass@prod_host:port/db_name'